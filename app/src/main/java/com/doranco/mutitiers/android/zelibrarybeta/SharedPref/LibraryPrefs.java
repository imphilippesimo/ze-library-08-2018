package com.doranco.mutitiers.android.zelibrarybeta.SharedPref;

import org.androidannotations.annotations.sharedpreferences.SharedPref;

@SharedPref(SharedPref.Scope.UNIQUE)
public interface LibraryPrefs {

    String loggedUsername();

    String token();

    String serverMessage();
}
