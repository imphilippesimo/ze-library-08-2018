package com.doranco.mutitiers.android.zelibrarybeta.rest;

import com.doranco.mutitiers.android.zelibrarybeta.model.Note;
import com.doranco.mutitiers.android.zelibrarybeta.utils.LibraryConstants;

import org.androidannotations.rest.spring.annotations.Get;
import org.androidannotations.rest.spring.annotations.Path;
import org.androidannotations.rest.spring.annotations.Rest;
import org.androidannotations.rest.spring.api.RestClientErrorHandling;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.List;

@Rest(converters = MappingJackson2HttpMessageConverter.class,rootUrl = LibraryConstants.ROOT_URL)
public interface NoteClient  extends RestClientErrorHandling {

    @Get("notes?bookId={bookId}&pageSize={pageSize}&pageNumber={pageNumber}")
    ResponseEntity<List<Note>> getNotes(@Path long bookId, @Path int pageSize, @Path int pageNumber);
}
