package com.doranco.mutitiers.android.zelibrarybeta.utils;

public class LibraryConstants {

    public static final String USERNAME_PATTERN = "^[a-zA-Z0-9_-]{6,15}$";
    public static final String PASSWORD_PATTERN = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,15}$";
    public static final String EXTRA_USERNAME = "username";
    public static final String ROOT_URL = "http://10.0.0.159:8080/library-web/webapi/";
    public static final String RESPONSE_SERVER_MESSAGE = "Server Message";
}
