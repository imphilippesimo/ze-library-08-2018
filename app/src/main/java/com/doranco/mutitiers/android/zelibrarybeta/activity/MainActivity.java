package com.doranco.mutitiers.android.zelibrarybeta.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.doranco.mutitiers.android.zelibrarybeta.R;
import com.doranco.mutitiers.android.zelibrarybeta.SharedPref.LibraryPrefs_;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.sharedpreferences.Pref;

@EActivity
public class MainActivity extends AppCompatActivity {

    @Pref
    LibraryPrefs_ lbPrefs;

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        intent = new Intent(this, NoteListActivity_.class);

//        if (lbPrefs.loggedUsername().exists()) {
//
//            intent = new Intent(this, BookListActivity_.class);
//
//        } else {
//            intent = new Intent(this, LoginActivity_.class);
//
//        }
        startActivity(intent);
        this.finish();

    }


}
