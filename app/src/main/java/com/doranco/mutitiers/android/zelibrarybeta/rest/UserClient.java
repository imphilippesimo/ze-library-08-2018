package com.doranco.mutitiers.android.zelibrarybeta.rest;

import com.doranco.mutitiers.android.zelibrarybeta.model.Credentials;
import com.doranco.mutitiers.android.zelibrarybeta.model.User;
import com.doranco.mutitiers.android.zelibrarybeta.utils.LibraryConstants;

import org.androidannotations.rest.spring.annotations.Body;
import org.androidannotations.rest.spring.annotations.Post;
import org.androidannotations.rest.spring.annotations.Rest;
import org.androidannotations.rest.spring.api.RestClientErrorHandling;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

@Rest(rootUrl = LibraryConstants.ROOT_URL,converters = {MappingJackson2HttpMessageConverter.class})
public interface UserClient extends RestClientErrorHandling {

   @Post("users/login")
   ResponseEntity authenticate(@Body Credentials credentials);



   @Post("users")
   ResponseEntity<User> suscribe(@Body User user);
}
