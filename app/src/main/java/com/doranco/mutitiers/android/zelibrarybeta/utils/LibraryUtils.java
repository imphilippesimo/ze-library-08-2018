package com.doranco.mutitiers.android.zelibrarybeta.utils;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.view.View;
import android.widget.ProgressBar;

import com.doranco.mutitiers.android.zelibrarybeta.model.Book;
import com.doranco.mutitiers.android.zelibrarybeta.model.User;

import org.androidannotations.annotations.EBean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@EBean(scope = EBean.Scope.Singleton)
public class LibraryUtils {


    private List<User> dummyUsers = new ArrayList<>(Arrays.asList(new User(null, null, "philippe", "Philippe8")));


    public List<User> getDummyUsers() {
        return dummyUsers;
    }

    public void setDummyUsers(List<User> dummyUsers) {
        this.dummyUsers = dummyUsers;
    }

    public void addUser(User user) {
        this.dummyUsers.add(user);
    }

    private List<Book> books = new ArrayList<Book>();

    private static final int COUNT = 25;


    public LibraryUtils() {

        for (int i = 1; i <= COUNT; i++) {
            addItem(createDummyBook(i));
        }
    }

    private static Book createDummyBook(int position) {
        return new Book("ISBN " + position, "Un livre avec un ISBN " + position);
    }


    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show, Activity callingActivity, final ProgressBar progressViewToShowOrHide, final View viewToShowOrHide) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = callingActivity.getResources().getInteger(android.R.integer.config_shortAnimTime);

            viewToShowOrHide.setVisibility(show ? View.GONE : View.VISIBLE);
            viewToShowOrHide.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    viewToShowOrHide.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressViewToShowOrHide.setVisibility(show ? View.VISIBLE : View.GONE);
            progressViewToShowOrHide.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressViewToShowOrHide.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progressViewToShowOrHide.setVisibility(show ? View.VISIBLE : View.GONE);
            viewToShowOrHide.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


    private void addItem(Book item) {
        books.add(item);

    }


    public List<Book> getBooks() {
        return books;
    }
}
