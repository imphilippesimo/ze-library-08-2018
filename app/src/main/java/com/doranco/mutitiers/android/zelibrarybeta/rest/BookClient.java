package com.doranco.mutitiers.android.zelibrarybeta.rest;

import com.doranco.mutitiers.android.zelibrarybeta.model.Book;
import com.doranco.mutitiers.android.zelibrarybeta.utils.LibraryConstants;

import org.androidannotations.rest.spring.annotations.Get;
import org.androidannotations.rest.spring.annotations.Path;
import org.androidannotations.rest.spring.annotations.Rest;
import org.androidannotations.rest.spring.api.RestClientErrorHandling;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.List;

@Rest(converters = MappingJackson2HttpMessageConverter.class,rootUrl = LibraryConstants.ROOT_URL)
public interface BookClient extends RestClientErrorHandling {

    @Get("books/search/{queryString}?pageSize={pageSize}&pageNumber={pageNumber}")
    ResponseEntity<List<Book>> getBooks(@Path String queryString, @Path int pageNumber, @Path int pageSize);
}
