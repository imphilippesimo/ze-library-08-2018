//package com.doranco.mutitiers.android.zelibrarybeta.activity;
//
//import android.app.Activity;
//import android.content.Context;
//import android.support.annotation.NonNull;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.ProgressBar;
//import android.widget.TextView;
//
//import com.doranco.mutitiers.android.zelibrarybeta.R;
//import com.doranco.mutitiers.android.zelibrarybeta.model.Book;
//import com.doranco.mutitiers.android.zelibrarybeta.utils.LibraryUtils;
//
//import org.androidannotations.annotations.Bean;
//import org.androidannotations.annotations.EBean;
//import org.androidannotations.annotations.RootContext;
//
//import java.util.List;
//
///**
// * {@link RecyclerView.Adapter} that can display a {@link Book}
// * TODO: Replace the implementation with code for your data type.
// */
//
//public class BookRecyclerViewAdapter extends RecyclerView.Adapter {
//
//    private final List<Book> mValues;
//    private static final int BOOK_LIST_TITLE_TYPE = 0;
//    private int BOOK_ITEM_TYPE = 1;
//
//
//
//
//    public BookRecyclerViewAdapter(List<Book> items) {
//        mValues = items;
//
//
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//        if (position == 0)
//            return BOOK_LIST_TITLE_TYPE;
//        else
//            return BOOK_ITEM_TYPE;
//    }
//
//    @Override
//    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view;
//        if (viewType == BOOK_LIST_TITLE_TYPE) {
//            view = LayoutInflater.from(parent.getContext())
//                    .inflate(R.layout.book_list_title_item, parent, false);
//            return new NoteListTitleViewHolder(view);
//
//        } else {
//            view = LayoutInflater.from(parent.getContext())
//                    .inflate(R.layout.book_item, parent, false);
//            return new NoteItemViewHolder(view);
//        }
//    }
//
//    @Override
//    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
//        if (position == 0) {
//
//            NoteListTitleViewHolder bookListTitleViewHolder = (NoteListTitleViewHolder) holder;
//            bookListTitleViewHolder.mNoteNumberView.setText(Integer.valueOf(mValues.size()).toString());
//
//            bookListTitleViewHolder.mView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    //TODO do something upon list title click
//                }
//            });
//        } else {
//            NoteItemViewHolder bookItemViewHolder = (NoteItemViewHolder) holder;
//            bookItemViewHolder.mTitleView.setText(mValues.get(position - 1).getTitle());
//            bookItemViewHolder.mPrefaceView.setText(mValues.get(position - 1).getPreface());
//            lbUtils.showProgress(false, mActivity, bookItemViewHolder.mProgressView, bookItemViewHolder.mNotesView);
//            Double mean = mValues.get(position - 1).getMean();
//            View notesView = bookItemViewHolder.mNotesView;
//            if (mean >= 0) {
//                notesView.findViewById(R.id.star_1).setBackground(mActivity.getDrawable(R.drawable.star));
//            }
//            if (mean > 1) {
//                notesView.findViewById(R.id.star_2).setBackground(mActivity.getDrawable(R.drawable.star));
//            }
//
//            if (mean > 2) {
//                notesView.findViewById(R.id.star_3).setBackground(mActivity.getDrawable(R.drawable.star));
//            }
//
//            if (mean > 3) {
//                notesView.findViewById(R.id.star_4).setBackground(mActivity.getDrawable(R.drawable.star));
//            }
//
//            if (mean > 4) {
//                notesView.findViewById(R.id.star_5).setBackground(mActivity.getDrawable(R.drawable.star));
//            }
//
//
//            bookItemViewHolder.mView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    //TODO do something upon click
//                }
//            });
//
//        }
//    }
//
//
//    @Override
//    public int getItemCount() {
//        return 1 + mValues.size();
//    }
//
//    public class NoteListTitleViewHolder extends RecyclerView.ViewHolder {
//        public final View mView;
//        public final TextView mNoteNumberView;
//
//
//        public NoteListTitleViewHolder(View view) {
//            super(view);
//            mView = view;
//            mNoteNumberView = view.findViewById(R.id.book_list_number);
//        }
//
//        @Override
//        public String toString() {
//            return super.toString() + " '" + mNoteNumberView.getText() + "'";
//        }
//    }
//
//    public class NoteItemViewHolder extends RecyclerView.ViewHolder {
//        public final View mView;
//        public final TextView mTitleView;
//        public final TextView mPrefaceView;
//        public final LinearLayout mNotesView;
//        public final ProgressBar mProgressView;
//
//
//        public NoteItemViewHolder(View view) {
//            super(view);
//            mView = view;
//            mTitleView = view.findViewById(R.id.book_title);
//            mPrefaceView = view.findViewById(R.id.book_preface);
//            mNotesView = view.findViewById(R.id.notes_linear_layout);
//            mProgressView = view.findViewById(R.id.notes_progress);
//        }
//
//        @Override
//        public String toString() {
//            return super.toString() + " '" + mTitleView.getText() + "'";
//        }
//    }
//
//
//}
