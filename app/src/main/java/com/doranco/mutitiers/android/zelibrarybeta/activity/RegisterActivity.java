package com.doranco.mutitiers.android.zelibrarybeta.activity;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.doranco.mutitiers.android.zelibrarybeta.R;
import com.doranco.mutitiers.android.zelibrarybeta.SharedPref.LibraryPrefs_;
import com.doranco.mutitiers.android.zelibrarybeta.model.User;
import com.doranco.mutitiers.android.zelibrarybeta.rest.UserClient;
import com.doranco.mutitiers.android.zelibrarybeta.utils.LibraryConstants;
import com.doranco.mutitiers.android.zelibrarybeta.utils.LibraryUtils;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.http.ResponseEntity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@EActivity
public class RegisterActivity extends AppCompatActivity {

    @ViewById(R.id.firstname)
    protected AutoCompleteTextView mFirstnameView;
    @ViewById(R.id.lastname)
    protected AutoCompleteTextView mLastnameView;
    @ViewById(R.id.username)
    protected AutoCompleteTextView mUsernameView;
    @ViewById(R.id.password)
    protected EditText mPasswordView;
    @ViewById(R.id.repeated_password)
    protected EditText mRepeatedPasswordView;
    @ViewById(R.id.register_progress)
    protected ProgressBar mProgressView;

    @ViewById(R.id.register_toolbar)
    protected Toolbar mRegisterToolbar;

    @ViewById(R.id.register_form)
    protected View mRegisterFormView;

    @RestService
    protected UserClient userClient;

    ActionBar actionBar;


    @Bean
    LibraryUtils lbUtils;

    @Pref
    LibraryPrefs_ prefs;
    private boolean mRegistrationSuccess;
    private User mUser;
    private ResponseEntity<User> mResponse;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        setSupportActionBar(mRegisterToolbar);
        actionBar = getSupportActionBar();

        // Enable the Up button
        actionBar.setDisplayHomeAsUpEnabled(true);



        mUsernameView.setText(getIntent().getStringExtra(LibraryConstants.EXTRA_USERNAME));


    }

    @Click(R.id.register_button)
    protected void attemptRegistration() {

        mUser = new User(mFirstnameView.getText().toString(), mLastnameView.getText().toString()
                , mUsernameView.getText().toString(), mPasswordView.getText().toString());

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(mUser.getFirstName())) {
            mFirstnameView.setError(getString(R.string.error_field_required));
            focusView = mFirstnameView;
            cancel = true;

        }

        if (TextUtils.isEmpty(mUser.getLastName())) {
            mLastnameView.setError(getString(R.string.error_field_required));
            focusView = mLastnameView;
            cancel = true;

        }

        // Check for a valid username address.
        if (TextUtils.isEmpty(mUser.getUserName())) {
            mUsernameView.setError(getString(R.string.error_field_required));
            focusView = mUsernameView;
            cancel = true;
        } else if (!isUsernameValid(mUser.getUserName())) {
            mUsernameView.setError(getString(R.string.error_invalid_username));
            focusView = mUsernameView;
            cancel = true;
        }

        if (TextUtils.isEmpty(mUser.getPassword())) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        } else if (!isPasswordValid(mUser.getPassword())) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        String repeatedPassword = mRepeatedPasswordView.getText().toString();

        if (TextUtils.isEmpty(repeatedPassword)) {
            mRepeatedPasswordView.setError(getString(R.string.error_field_required));
            focusView = mRepeatedPasswordView;
            cancel = true;
        } else if (!TextUtils.equals(repeatedPassword, mUser.getPassword())) {
            mRepeatedPasswordView.setError(getString(R.string.error_repeated_password_not_matching));
            focusView = mRepeatedPasswordView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            lbUtils.showProgress(true, this, mProgressView, mRegisterFormView);
            doRegistration();
        }


    }


    @Background
    protected void doRegistration() {

        //attempt authentication against a network service.

        mResponse =userClient.suscribe(mUser);

        if(mResponse!=null){
            mRegistrationSuccess = true;
        }

        onPostRegistration();


    }

    @UiThread
    protected void onPostRegistration() {
        if (mRegistrationSuccess) {
            Intent intent = new Intent(this, LoginActivity_.class);
            startActivity(intent);

        } else {
            Snackbar.make(mRegisterFormView,prefs.serverMessage().get(), Snackbar.LENGTH_SHORT);
            mFirstnameView.requestFocus();
        }

        lbUtils.showProgress(false, this, mProgressView, mRegisterFormView);
    }


    private boolean isUsernameValid(String username) {
        return matches(LibraryConstants.USERNAME_PATTERN, username);
    }

    private boolean isPasswordValid(String password) {
        return matches(LibraryConstants.PASSWORD_PATTERN, password);
    }

    private boolean matches(String stringPattern, String stringToMatch) {
        Pattern pattern = Pattern.compile(stringPattern);
        Matcher matcher = pattern.matcher(stringToMatch);
        return matcher.matches();

    }

}
