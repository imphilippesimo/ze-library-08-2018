package com.doranco.mutitiers.android.zelibrarybeta.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.doranco.mutitiers.android.zelibrarybeta.R;
import com.doranco.mutitiers.android.zelibrarybeta.SharedPref.LibraryPrefs_;
import com.doranco.mutitiers.android.zelibrarybeta.model.Credentials;
import com.doranco.mutitiers.android.zelibrarybeta.rest.UserClient;
import com.doranco.mutitiers.android.zelibrarybeta.rest.errorHandler.LibraryErrorHandler;
import com.doranco.mutitiers.android.zelibrarybeta.utils.LibraryConstants;
import com.doranco.mutitiers.android.zelibrarybeta.utils.LibraryUtils;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.http.ResponseEntity;

/**
 * A login screen that offers login via email/password.
 */
@EActivity
public class LoginActivity extends AppCompatActivity {


    // UI references.
    @ViewById(R.id.username)
    protected AutoCompleteTextView mUsernameView;
    @ViewById(R.id.password)
    protected EditText mPasswordView;

    @ViewById(R.id.login_toolbar)
    protected Toolbar mToolbar;

    @ViewById(R.id.login_progress)
    protected ProgressBar mProgressView;

    @ViewById(R.id.login_form)
    protected View mLoginFormView;

    @Bean
    protected LibraryUtils lbUtils;

    @Pref
    protected LibraryPrefs_ prefs;

    @RestService
    UserClient userClient;

    @Bean
    LibraryErrorHandler libraryErrorHandler;


    private Pattern pattern;
    private Matcher matcher;

    private boolean mLoginSuccess = false;
    private String mUsername;
    private String mPassword;
    private ResponseEntity mResponse;
    private String TAG = "LoginActivity";


    @AfterInject
    void afterInject() {
        userClient.setRestErrorHandler(libraryErrorHandler);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setSupportActionBar(mToolbar);


        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });
    }


    @Click(R.id.sign_in_button)
    protected void attemptLogin() {

        // Reset errors.
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        mUsername = mUsernameView.getText().toString();
        mPassword = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(mPassword) && !isPasswordValid(mPassword)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid username address.
        if (TextUtils.isEmpty(mUsername)) {
            mUsernameView.setError(getString(R.string.error_field_required));
            focusView = mUsernameView;
            cancel = true;
        } else if (!isUsernameValid(mUsername)) {
            mUsernameView.setError(getString(R.string.error_invalid_username));
            focusView = mUsernameView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            lbUtils.showProgress(true, this, mProgressView, mLoginFormView);
            doLogin();


        }
    }

    @Background
    protected void doLogin() {
        // attempt authentication against a network service.
        Credentials credentials = new Credentials(mUsername, mPassword);

        mResponse = userClient.authenticate(credentials);

        if (mResponse != null) {
            mLoginSuccess = true;
        }

        onPostLogin();

    }

    @UiThread
    protected void onPostLogin() {

        if (mLoginSuccess) {
            String token = mResponse.getHeaders().getAuthorization();
            prefs.loggedUsername().put(mUsername);
            prefs.token().put(token);
            Log.d(TAG, "JWT >>>>>>>> :" + prefs.token().get());
            Log.d(TAG, "JWT >>>>>>>> :" + prefs.loggedUsername().exists());
            Log.d(TAG, "JWT >>>>>>>> :" + prefs.loggedUsername().get());
            startActivity(new Intent(this, BookListActivity_.class));

        } else {
            mUsernameView.requestFocus();
        }
        lbUtils.showProgress(false, this, mProgressView, mLoginFormView);

    }

    private boolean isUsernameValid(String username) {
        return matches(LibraryConstants.USERNAME_PATTERN, username);
    }

    private boolean isPasswordValid(String password) {
        return matches(LibraryConstants.PASSWORD_PATTERN, password);
    }

    private boolean matches(String stringPattern, String stringToMatch) {
        pattern = Pattern.compile(stringPattern);
        matcher = pattern.matcher(stringToMatch);
        return matcher.matches();

    }

    @Click(R.id.goto_registration_button)
    protected void gotoRegistration() {
        Intent intent = new Intent(this, RegisterActivity_.class);
        intent.putExtra(LibraryConstants.EXTRA_USERNAME, mUsernameView.getText().toString());
        startActivity(intent);

    }


}

