package com.doranco.mutitiers.android.zelibrarybeta.activity;

import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.doranco.mutitiers.android.zelibrarybeta.R;
import com.doranco.mutitiers.android.zelibrarybeta.model.Book;
import com.doranco.mutitiers.android.zelibrarybeta.rest.BookClient;
import com.doranco.mutitiers.android.zelibrarybeta.rest.errorHandler.LibraryErrorHandler;
import com.doranco.mutitiers.android.zelibrarybeta.utils.LibraryUtils;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;


@EActivity
public class BookListActivity extends AppCompatActivity {

    private static final String LOG_TAG = "BookListActivity";
    @ViewById(R.id.book_list_swiperefresh)
    SwipeRefreshLayout mRefreshLayout;

    @ViewById(R.id.book_list_rv)
    protected RecyclerView bookListRecyclerView;

    @ViewById(R.id.book_list_toolbar)
    protected Toolbar mToolbar;

    @ViewById(R.id.book_list_progress)
    protected ProgressBar mProgressBar;

    List<Book> mBooks = new ArrayList<Book>();
    List<Book> mBooksCopy = new ArrayList<Book>();

    private RecyclerView.LayoutManager mLayoutManager;

    private BookRecyclerViewAdapter mAdapter;

    @Bean
    LibraryUtils lbUtils;

    @RestService
    BookClient mBookClient;

    @Bean
    LibraryErrorHandler mErrorHandler;


    Activity thisActivity;

    Boolean mLoadBooksSucceed = false;

    @AfterInject
    void afterInject() {
        mBookClient.setRestErrorHandler(mErrorHandler);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_list);

        setSupportActionBar(mToolbar);

        lbUtils.showProgress(true, this, mProgressBar, bookListRecyclerView);

        /*
         * Sets up a SwipeRefreshLayout.OnRefreshListener that is invoked when the user
         * performs a swipe-to-refresh gesture.
         */
        mRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        Log.i(LOG_TAG, "onRefresh called from SwipeRefreshLayout");

                        //TODO REFRESH BOOK LIST
                        doRefreshBooks();
                    }
                }
        );


        mLayoutManager = new LinearLayoutManager(this);
        bookListRecyclerView.setLayoutManager(mLayoutManager);

        thisActivity = this;

        loadBooks();


    }

    @Background
    protected void loadBooks() {
        ResponseEntity<List<Book>> response = mBookClient.getBooks("_", 0, 20);
        if (response != null) {
            mBooks.addAll(response.getBody());
            mBooksCopy.addAll(mBooks);
            mLoadBooksSucceed = true;
        }

        onPostLoadBooks();
    }

    @UiThread
    protected void onPostLoadBooks() {
        lbUtils.showProgress(false, this, mProgressBar, bookListRecyclerView);
        mAdapter = new BookRecyclerViewAdapter(mBooks);
        bookListRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.book_list_menu, menu);

        final MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView =
                (SearchView) searchItem.getActionView();


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {

                Log.d(LOG_TAG, "text submitted: " + s);
                List<Book> resultBooks = new ArrayList<Book>();
                for (Book book : mBooksCopy) {
                    if (book.getTitle().contains(s)) {
                        Log.d(LOG_TAG, "found a book");
                        resultBooks.add(book);
                    }
                }

                MenuItem searchMenuItem = menu.findItem(R.id.search);

                searchMenuItem.collapseActionView();
                refreshLayout(resultBooks);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                Log.d(LOG_TAG, "text changed:" + s);
                return true;
            }
        });


        MenuItemCompat.OnActionExpandListener expandListener = new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem menuItem) {

                return true;  // Return true to collapse action view
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem menuItem) {

                refreshLayout(mBooksCopy);
                return true;  // Return true to collapse action view
            }
        };

        // Get the MenuItem for the action item
        MenuItem searchMenuItem = menu.findItem(R.id.search);

        // Assign the listener to that action item
        MenuItemCompat.setOnActionExpandListener(searchMenuItem, expandListener);


        return true;

    }

    private void refreshLayout(List<Book> newBooks) {

        mBooks.clear();
        mBooks.addAll(newBooks);
        onDataChanged();
    }

    @UiThread
    protected void onDataChanged() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                Log.d(LOG_TAG, "User chose the 'Settings' item, show the app settings UI...");
                return true;

            case R.id.refresh:
                Log.d(LOG_TAG, "User clicked on refresh action");
                refreshLayout(mBooksCopy);
                return true;

            case R.id.help:
                Log.d(LOG_TAG, "User clicked on help action");
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    private void doRefreshBooks() {

//        try {
//            Thread.sleep(2000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        refreshLayout(mBooksCopy);

        //TODO trigger refreshing
        mRefreshLayout.setRefreshing(false);


    }


    public class BookRecyclerViewAdapter extends RecyclerView.Adapter {

        private final List<Book> mValues;
        private static final int BOOK_LIST_TITLE_TYPE = 0;
        private int BOOK_ITEM_TYPE = 1;
        private View mNotesView;


        public BookRecyclerViewAdapter(List<Book> items) {
            mValues = items;


        }

        @Override
        public int getItemViewType(int position) {
            if (position == 0)
                return BOOK_LIST_TITLE_TYPE;
            else
                return BOOK_ITEM_TYPE;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view;
            if (viewType == BOOK_LIST_TITLE_TYPE) {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.book_list_title_item, parent, false);
                return new BookListTitleViewHolder(view);

            } else {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.book_item, parent, false);
                return new BookItemViewHolder(view);
            }
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {


            if (position == 0) {

                BookListTitleViewHolder bookListTitleViewHolder = (BookListTitleViewHolder) holder;
                bookListTitleViewHolder.mBookNumberView.setText(Integer.valueOf(mValues.size()).toString());

                bookListTitleViewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //TODO do something upon list title click
                    }
                });
            } else {
                Book currentBook = mValues.get(position - 1);
                BookItemViewHolder bookItemViewHolder = (BookItemViewHolder) holder;
                bookItemViewHolder.mTitleView.setText(currentBook.getTitle());
                bookItemViewHolder.mPrefaceView.setText(currentBook.getPreface());
                byte[] image = currentBook.getImage();
                bookItemViewHolder.mImageView.setBackground(null);
                if (image != null) {
                    Drawable dImage = new BitmapDrawable(getResources(), BitmapFactory.decodeByteArray(image, 0, image.length));
                    bookItemViewHolder.mImageView.setBackground(dImage);
                }

                lbUtils.showProgress(false, thisActivity, bookItemViewHolder.mProgressView, bookItemViewHolder.mNotesView);
                Double mean = mValues.get(position - 1).getMean();
                mNotesView = bookItemViewHolder.mNotesView;
                grayStars();
                if (mean > 0) {
                    mNotesView.findViewById(R.id.star_1).setBackground(thisActivity.getDrawable(R.drawable.star));
                }

                if (mean > 1) {
                    mNotesView.findViewById(R.id.star_2).setBackground(thisActivity.getDrawable(R.drawable.star));
                }

                if (mean > 2) {
                    mNotesView.findViewById(R.id.star_3).setBackground(thisActivity.getDrawable(R.drawable.star));
                }

                if (mean > 3) {
                    mNotesView.findViewById(R.id.star_4).setBackground(thisActivity.getDrawable(R.drawable.star));
                }

                if (mean > 4) {
                    mNotesView.findViewById(R.id.star_5).setBackground(thisActivity.getDrawable(R.drawable.star));
                }


                bookItemViewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //TODO do something upon click
                    }
                });

            }
        }

        private void grayStars() {

            mNotesView.findViewById(R.id.star_1).setBackground(thisActivity.getDrawable(R.drawable.star_gray));
            mNotesView.findViewById(R.id.star_2).setBackground(thisActivity.getDrawable(R.drawable.star_gray));
            mNotesView.findViewById(R.id.star_3).setBackground(thisActivity.getDrawable(R.drawable.star_gray));
            mNotesView.findViewById(R.id.star_4).setBackground(thisActivity.getDrawable(R.drawable.star_gray));
            mNotesView.findViewById(R.id.star_5).setBackground(thisActivity.getDrawable(R.drawable.star_gray));

        }


        @Override
        public int getItemCount() {
            return 1 + mValues.size();
        }

        public class BookListTitleViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final TextView mBookNumberView;


            public BookListTitleViewHolder(View view) {
                super(view);
                mView = view;
                mBookNumberView = view.findViewById(R.id.book_list_number);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + mBookNumberView.getText() + "'";
            }
        }

        public class BookItemViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final TextView mTitleView;
            public final TextView mPrefaceView;
            public final LinearLayout mNotesView;
            public final ProgressBar mProgressView;
            public final ImageView mImageView;


            public BookItemViewHolder(View view) {
                super(view);
                mView = view;
                mTitleView = view.findViewById(R.id.book_title);
                mPrefaceView = view.findViewById(R.id.book_preface);
                mNotesView = view.findViewById(R.id.notes_linear_layout);
                mProgressView = view.findViewById(R.id.notes_progress);
                mImageView = view.findViewById(R.id.book_image);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + mTitleView.getText() + "'";
            }
        }


    }


}
