package com.doranco.mutitiers.android.zelibrarybeta.model;

public class Credentials {

    private String userName;
    private String pwd;

    public Credentials() {
    }

    public Credentials(String userName, String pwd) {
        this.userName = userName;
        this.pwd = pwd;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
}
