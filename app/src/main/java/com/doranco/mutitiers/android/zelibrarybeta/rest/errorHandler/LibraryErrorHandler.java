package com.doranco.mutitiers.android.zelibrarybeta.rest.errorHandler;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.doranco.mutitiers.android.zelibrarybeta.R;
import com.doranco.mutitiers.android.zelibrarybeta.SharedPref.LibraryPrefs_;
import com.doranco.mutitiers.android.zelibrarybeta.activity.LoginActivity_;
import com.doranco.mutitiers.android.zelibrarybeta.utils.LibraryConstants;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.androidannotations.rest.spring.api.RestErrorHandler;
import org.springframework.core.NestedRuntimeException;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;

@EBean
public class LibraryErrorHandler implements RestErrorHandler {

    @RootContext
    Context context;

    @Pref
    LibraryPrefs_ prefs;
    private String serverMessage;


    @Override
    public void onRestClientExceptionThrown(NestedRuntimeException e) {


        if (e instanceof ResourceAccessException) {
            serverMessage = context.getResources().getString(R.string.network_access_error);
        } else {
            HttpClientErrorException httpE = (HttpClientErrorException) e;
            serverMessage = httpE.getResponseHeaders().getFirst(LibraryConstants.RESPONSE_SERVER_MESSAGE);

        }

        prefs.serverMessage().put(serverMessage);

    }


}
