package com.doranco.mutitiers.android.zelibrarybeta.activity;

import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.doranco.mutitiers.android.zelibrarybeta.R;
import com.doranco.mutitiers.android.zelibrarybeta.model.Book;
import com.doranco.mutitiers.android.zelibrarybeta.model.Note;
import com.doranco.mutitiers.android.zelibrarybeta.rest.NoteClient;
import com.doranco.mutitiers.android.zelibrarybeta.utils.LibraryUtils;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

@EActivity
public class NoteListActivity extends AppCompatActivity {

    private static final String LOG_TAG = "NoteListActivity";
    private static final int NOTE_LIST_TITLE_TYPE = 0;
    private static final int NOTE_ITEM_TYPE = 1;
    @Bean
    LibraryUtils lbUtils;

    @ViewById(R.id.note_list_swiperefresh)
    SwipeRefreshLayout mRefreshLayout;

    @ViewById(R.id.note_list_rv)
    protected RecyclerView noteListRecyclerView;

    @ViewById(R.id.note_list_toolbar)
    protected Toolbar mToolbar;

    @ViewById(R.id.note_list_progress)
    protected ProgressBar mProgressBar;

    List<Note> mNotes = new ArrayList<Note>();
    List<Note> mNotesCopy = new ArrayList<Note>();

    private RecyclerView.LayoutManager mLayoutManager;

    private NoteListActivity.NoteRecyclerViewAdapter mAdapter;
    private NoteListActivity thisActivity;

    @RestService
    NoteClient mNoteClient;
    private boolean mLoadNotesSucceed = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_list);

        setSupportActionBar(mToolbar);

        lbUtils.showProgress(true, this, mProgressBar, noteListRecyclerView);

        /*
         * Sets up a SwipeRefreshLayout.OnRefreshListener that is invoked when the user
         * performs a swipe-to-refresh gesture.
         */
        mRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        Log.i(LOG_TAG, "onRefresh called from SwipeRefreshLayout");

                        //TODO REFRESH BOOK LIST
                        doRefreshBooks();
                    }
                }
        );

        setSupportActionBar(mToolbar);

        lbUtils.showProgress(true, this, mProgressBar, noteListRecyclerView);

        mLayoutManager = new LinearLayoutManager(this);
        noteListRecyclerView.setLayoutManager(mLayoutManager);

        thisActivity = this;

        loadNotes();
    }


    @Background
    protected void loadNotes() {
        ResponseEntity<List<Note>> response = mNoteClient.getNotes(12L, 10, 0);
        if (response != null) {
            mNotes.addAll(response.getBody());
            mNotesCopy.addAll(mNotes);
            mLoadNotesSucceed = true;
        }

        onPostLoadNotes();
    }

    @UiThread
    protected void onPostLoadNotes() {
        lbUtils.showProgress(false, this, mProgressBar, noteListRecyclerView);
        mAdapter = new NoteListActivity.NoteRecyclerViewAdapter(mNotes);
        noteListRecyclerView.setAdapter(mAdapter);
    }

    private void doRefreshBooks() {
    }

    @UiThread
    protected void onDataChanged() {
        mAdapter.notifyDataSetChanged();
    }

    public class NoteRecyclerViewAdapter extends RecyclerView.Adapter {

        private final List<Note> mValues;


        public NoteRecyclerViewAdapter(List<Note> items) {
            mValues = items;
        }

        @Override
        public int getItemViewType(int position) {
            if (position == 0)
                return NOTE_LIST_TITLE_TYPE;
            else
                return NOTE_ITEM_TYPE;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view;
            if (viewType == NOTE_LIST_TITLE_TYPE) {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.note_list_header, parent, false);
                return new NoteListTitleViewHolder(view);

            } else {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.note_item, parent, false);
                return new NoteItemViewHolder(view);
            }
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {


            if (position == 0) {

                NoteListActivity.NoteRecyclerViewAdapter.NoteListTitleViewHolder noteListTitleViewHolder = (NoteListTitleViewHolder) holder;
                noteListTitleViewHolder.mNoteNumberView.setText(Integer.valueOf(mValues.size()).toString());

                noteListTitleViewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //TODO do something upon list title click
                    }
                });
            } else {
                Note currentNote = mValues.get(position - 1);
                NoteListActivity.NoteRecyclerViewAdapter.NoteItemViewHolder noteListTitleViewHolder = (NoteItemViewHolder) holder;
                noteListTitleViewHolder.mNoteUsername.setText(currentNote.getUsername());
                noteListTitleViewHolder.mNoteComment.setText(currentNote.getComment());
            }
        }


        @Override
        public int getItemCount() {
            return 1 + mValues.size();
        }

        public class NoteListTitleViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final TextView mBookTitleView;
            public final TextView mNoteNumberView;


            public NoteListTitleViewHolder(View view) {
                super(view);
                mView = view;
                mNoteNumberView = view.findViewById(R.id.note_list_number);
                mBookTitleView = view.findViewById(R.id.note_list_title);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + mNoteNumberView.getText() + "'";
            }
        }

        public class NoteItemViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final TextView mNoteUsername;
            public final TextView mNoteComment;


            public NoteItemViewHolder(View view) {
                super(view);
                mView = view;
                mNoteUsername = view.findViewById(R.id.note_user_name_text_view);
                mNoteComment = view.findViewById(R.id.note_comment_text_view);
            }


        }

    }
}
